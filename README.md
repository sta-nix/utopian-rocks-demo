# utopian-rocks-demo
## Branch patch
https://youtu.be/2N75m6Pq9eY
## Playlist
https://www.youtube.com/watch?v=kWbighSj7V8&list=PLJbE2Yu2zumDqr_-hqpAN0nIr6m14TAsd&index=69
### In this tutorial, we take a look at the basic concepts of a program.  We create a simple BLoC to bring in our main data structures. 

### To build this application, you will need the latest version of the flutter SDK.  
### Check out the Youtube Tutorial for this [Dart Flutter Program](https://youtu.be/kWbighSj7V8). Here is our [Youtube Channel](https://www.youtube.com/channel/UCYqCZOwHbnPwyjawKfE21wg) Subscribe for more content.

### Check out our blog at [tensor-programming.com](http://tensor-programming.com/).

### Our [Twitter](https://twitter.com/TensorProgram), our [facebook](https://www.facebook.com/Tensor-Programming-1197847143611799/) and our [Steemit](https://steemit.com/@tensor).
